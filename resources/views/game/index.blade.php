@extends('layouts/master')
@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Game
      <a href="/game/create"><button class="btn btn-primary btn-sm float-right mr-6" type="button"><i class="fas fa-plus" ></i> Tambah Data</button></a>
    </h3>
  </div>
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>    
          <strong>{{ $message }}</strong>
      </div>
    @endif
  <div class="card-body">
        <table class="table" id="example1">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Developer</th>
                <th scope="col">Gameplay</th>
                <th scope="col">Year</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($game as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->developer}}</td>
                        <td>{{$value->gameplay}}</td>
                        <td>{{$value->year}}</td>
                        <td>
                            
                            <form action="/game/{{$value->id}}" method="POST">
                                @csrf
                                <a href="/game/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/game/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
  </div>
  <!-- /.card-body -->
</div>
    
@endsection