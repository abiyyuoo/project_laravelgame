@extends('layouts.master');

@section('content')

    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <h2>Edit Game {{$game->id}}</h2>
            <form action="/game/{{$game->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="{{$game->name}}" id="name" placeholder="Masukkan Nama">
                    @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="developer">Developer</label>
                    <input type="text" class="form-control" name="developer"  value="{{$game->developer}}"  id="developer" placeholder="Masukkan Developer">
                    @error('developer')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="gameplay">Gameplay</label>
                    <textarea name="gameplay" id="gameplay" class="form-control" cols="30" rows="10">{{$game->gameplay}}
                    </textarea>
                    @error('gameplay')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="year">Tahun</label>
                    <input type="date" class="form-control" name="year"  value="{{$game->year}}"  id="year" >
                    @error('year')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
          
            
        <!-- /.card-body -->
      </div>
    
@endsection
