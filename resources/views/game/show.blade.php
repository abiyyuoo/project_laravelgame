@extends('layouts/master')
@section('content')

  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-body">
        <table class="table table-bordered">
            <h3 hidden>ID : {{$game->id}}</h3>
            <h1 class="text-primary">{{$game->name}} ({{$game->year}})</h1>
            <p>Developer : {{$game->developer}}</p>
            <h4 class="text-dark">Gameplay</h4>
            <p>{{$game->gameplay}}</p>
            <h4 class="text-dark">Platform</h4>
            <button type="button" class="btn btn-primary btn-sm">{{$platform->name}}</button> &nbsp;
            
        </table>
      </div>


      <!-- /.card-body -->
      
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->
  
  </section>
@endsection

