@extends('layouts.master');

@section('content')


<div>
    <h2>Tambah Data</h2>
        <form action="/game" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" class="form-control" name="developer" id="developer" placeholder="Masukkan Developer">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gameplay">Gameplay</label>
                <textarea name="gameplay" id="gameplay" class="form-control" cols="30" rows="10"></textarea>
                {{-- <input type="text" class="form-control" name="gameplay" id="gameplay" placeholder="Masukkan Gameplay"> --}}
                @error('gameplay')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="date" class="form-control" name="year" id="year">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        
        
</div>
    
@endsection