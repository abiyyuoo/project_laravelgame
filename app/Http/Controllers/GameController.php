<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function gambar()
    {
        return view('game.gambar');
    }

    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'developer' => 'required',
            'gameplay' => 'required',
            'year' => 'required',
        ]);
        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "developer" => $request["developer"],
            "gameplay" => $request["gameplay"],
            "year" => $request["year"]
        ]);
        return redirect('/game')->with(['success' => 'Data Berhasil Di Tambah']);
        
    }

    public function index()
    {
        $game = DB::table('game')->get();
        return view('game.index', compact('game'));
    }

    public function show($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        $platform = DB::table('platform')->find($id);
        return view('game.show', compact('game','platform'));
    }
    

    public function edit($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        return view('game.edit', compact('game'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'developer' => 'required',
            'gameplay' => 'required',
            'year' => 'required',
        ]);

        $query = DB::table('game')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
                "developer" => $request["developer"],
                "gameplay" => $request["gameplay"],
                "year" => $request["year"]
            ]);
        return redirect('/game')->with(['success' => 'Data Berhasil Di Update']);
    }

    public function destroy($id)
    {
        $query = DB::table('game')->where('id', $id)->delete();
        return redirect('/game')->with(['success' => 'Data Dihapus']);
    }
}
